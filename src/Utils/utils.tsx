export function decode_utf8(s: string) {
  var doc = new DOMParser().parseFromString(s, 'text/html');
  return doc.documentElement.textContent;
}
