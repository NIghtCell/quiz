import React from 'react';
import './Categories.css';
import Card from '../Card/Card';
import { useHistory } from 'react-router-dom';
interface Category {
  title: string;
  number: number;
}

function Categories() {
  const history = useHistory();
  const categories: Category[] = [
    {
      title: 'Sports',
      number: 21,
    },
    {
      title: 'Animals',
      number: 27,
    },
    {
      title: 'Books',
      number: 10,
    },
    {
      title: 'History',
      number: 23,
    },
  ];

  const onClick = (categoryId: number) => {
    history.replace(`/quiz/${categoryId}`);
  };

  return (
    <div className="categories">
      <h3>Choose a category</h3>
      <div className="categories__list">
        {categories.map((category: Category) => (
          <Card
            onClick={() => onClick(category.number)}
            title={category.title}
          />
        ))}
      </div>
    </div>
  );
}

export default Categories;
