import React from 'react';
import './Question.css';
import { decode_utf8 } from '../../Utils/utils';

function Question({ question, onAnswerClick }: any) {
  const shuffle = (array: any) => {
    let counter = array.length;
    while (counter > 0) {
      let index = Math.floor(Math.random() * counter);
      counter--;
      let temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
    }

    return array;
  };

  const answers = [...question.incorrect_answers, question.correct_answer];
  shuffle(answers);
  const quizQuestion = decode_utf8(question.question);

  return (
    <div className="question">
      <p className="question__question">{quizQuestion}</p>
      <ul>
        {answers.map((answer: any) => (
          <li className="question__answer">
            <button onClick={() => onAnswerClick(answer)}>
              {decode_utf8(answer)}
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Question;
