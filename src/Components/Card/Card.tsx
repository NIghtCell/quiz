import React from 'react';
import './Card.css';

interface Props {
  onClick: () => void;
  title: string;
}

function Card({ title, onClick }: Props) {
  return (
    <div className="card" onClick={onClick}>
      <h5 className="card__title">{title}</h5>
    </div>
  );
}

export default Card;
