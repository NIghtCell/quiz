import React from 'react';
import './Result.css';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { decode_utf8 } from '../../Utils/utils';
import { useHistory } from 'react-router-dom';

interface QuestionAnswer {
  question: number;
  isCorrect: boolean;
  answer: string;
}

function Result({ questionAnswers, questions }: any) {
  const history = useHistory();
  const [showAnswers, setShowAnswers] = React.useState<boolean[]>(
    new Array(questions.length).fill(false)
  );

  const toggleShowAnswer = (questionNumber: number) => {
    const currentShowAnswers = showAnswers.map((answer, i) => {
      if (questionNumber === i) {
        if (showAnswers[i] === undefined) {
          return true;
        } else return !showAnswers[i];
      } else {
        return answer;
      }
    });

    setShowAnswers(currentShowAnswers);
  };

  return (
    <>
      <div className="results">
        <div className="results__controls">
          <ChevronLeftIcon onClick={() => history.replace('/categories')} />
          <h1>Results</h1>
        </div>
        <div className="result__answers">
          {questionAnswers.map((questionAnswer: QuestionAnswer) => (
            <div className="result__answer__container">
              <div
                onClick={() => toggleShowAnswer(questionAnswer.question)}
                className="result__answer"
              >
                <span className="result__answer__item">
                  {questionAnswer.question + 1}
                </span>
                <span className="result__answer__item">
                  Answer: {decode_utf8(questionAnswer.answer)}
                </span>
                <span className="result__answer__item">
                  Right:
                  {decode_utf8(
                    questions[questionAnswer.question].correct_answer
                  )}
                </span>
                <span className="result__answer__item">
                  {questionAnswer.isCorrect ? <CheckIcon /> : <CloseIcon />}
                </span>
              </div>
              {showAnswers[questionAnswer.question] && (
                <div className="result__question">
                  <span>
                    {decode_utf8(questions[questionAnswer.question].question)}
                  </span>
                </div>
              )}
            </div>
          ))}
        </div>
      </div>
    </>
  );
}

export default Result;
