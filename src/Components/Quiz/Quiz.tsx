import React from 'react';
import Question from '../Question/Question';
import './Quiz.css';
import Result from '../Result/Result';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { useHistory, useParams } from 'react-router-dom';

interface CorrectAnswer {
  question: number;
  isCorrect: boolean;
  answer: string;
}

interface Question {
  correct_answer: string;
}

function Quiz() {
  const history = useHistory();
  const { id } = useParams();
  const API_KEY = 'https://opentdb.com/api.php?amount=10&category=' + id;
  const [isLoaded, setIsLoaded] = React.useState(false);
  const [questions, setQuestions] = React.useState<Question[]>();
  const [error, setError] = React.useState(false);
  const [currentQuestion, setCurrentQuestion] = React.useState(0);
  const [questionAnswers, setquestionAnswers] = React.useState<CorrectAnswer[]>(
    []
  );
  const [isFinished, setIsFinished] = React.useState(false);

  const hasNextQuestion = questions && questions.length > currentQuestion + 1;

  React.useEffect(() => {
    fetch(API_KEY)
      .then((res) => res.json())
      .then(
        (result) => {
          setQuestions(result.results);
          setIsLoaded(true);
          setIsFinished(false);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
  }, [API_KEY]);

  const showNextQuestion = () => {
    if (hasNextQuestion) {
      setCurrentQuestion(currentQuestion + 1);
    } else {
      setIsFinished(true);
    }
  };

  const onNextQuestionClick = () => {
    setquestionAnswers([
      ...questionAnswers,
      {
        question: currentQuestion,
        isCorrect: false,
        answer: 'Keine',
      },
    ]);
    showNextQuestion();
  };

  const onAnswerClick = (answer: string) => {
    const currentCorrectAnswer: CorrectAnswer = {
      question: currentQuestion,
      isCorrect: isAnswerCorrect(answer),
      answer: answer,
    };
    setquestionAnswers([...questionAnswers, currentCorrectAnswer]);
    showNextQuestion();
  };

  const isAnswerCorrect = (answer: string) => {
    if (questions) {
      return questions[currentQuestion].correct_answer === answer;
    }
    return false;
  };

  return (
    <>
      {isFinished ? (
        <Result questionAnswers={questionAnswers} questions={questions} />
      ) : (
        <>
          {isLoaded && questions && !error && (
            <div className="quiz">
              <div className="quiz__content">
                <Question
                  onAnswerClick={onAnswerClick}
                  question={questions[currentQuestion]}
                />
                <div className="quiz__controls">
                  <button
                    onClick={() => history.replace('/categories')}
                    className="quiz__button quiz__button__back"
                  >
                    <ChevronLeftIcon /> Categories
                  </button>
                  <button
                    onClick={onNextQuestionClick}
                    className="quiz__button quiz__button__next"
                  >
                    Next Question <ChevronRightIcon />
                  </button>
                </div>
              </div>
            </div>
          )}
        </>
      )}
    </>
  );
}

export default Quiz;
