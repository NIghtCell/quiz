import React from 'react';
import './App.css';
import Categories from './Components/Categories/Categories';
import Quiz from './Components/Quiz/Quiz';
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route,
} from 'react-router-dom';

function App() {
  return (
    <div className="app">
      <Router>
        <Redirect to="/categories" />
        <Switch>
          <Route path="/categories">
            <Categories />
          </Route>
          <Route path="/quiz/:id">
            <Quiz />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
